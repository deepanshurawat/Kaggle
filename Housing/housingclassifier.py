import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import  LinearRegression
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.metrics import accuracy_score
#from sklearn.cross_validation import cross_val_score,cross_val_predict,StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
import  matplotlib.pyplot as plt

def handle_non_numerical_data(df):
    columns = df.columns.values
    categorical_mapping={}
    for column in columns:
        text_digit_vals = {}
        def convert_to_int(val):
            return text_digit_vals[val]

        if df[column].dtype != np.int64 and df[column].dtype != np.float64:
            column_contents = df[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x+=1

            df[column] = list(map(convert_to_int, df[column]))
            categorical_mapping[column]=text_digit_vals
    return df,categorical_mapping

df=pd.read_csv('train.csv')
#df,mapping=handle_non_numerical_data(df)
#print(mapping)
#print(len(mapping))
#print(list(mapping.keys()))

#print(df.head())

#df.info()
#df.isnull().sum()
missing_columns=df.isnull().sum()
missing_columns=missing_columns[missing_columns.values>0]
missing_columns=(missing_columns/1460)*100
NaN_columns=missing_columns.index.tolist()
drop_elements=['Alley','FireplaceQu','PoolQC','Fence','MiscFeature']
garage_elements=['GarageType','GarageYrBlt','GarageFinish','GarageQual','GarageCond']
basement_elements=['BsmtQual','BsmtCond','BsmtExposure','BsmtFinType1','BsmtFinType2']


for element in [drop_elements,garage_elements,basement_elements]:
    for j in element:
        NaN_columns.remove(j)

#NaN_columns
df.drop(drop_elements,axis=1).head()
#print(df.describe())
categorical_col=['MasVnrType','Electrical']
continious_col=['LotFrontage','MasVnrArea']

df[categorical_col].apply(lambda x:x.fillna(x.value_counts().index[0]))
df['MasVnrArea'].fillna(value=0,inplace=True)

df['LotFrontage'] = df['LotFrontage'].fillna(np.sqrt(df['LotArea']))
df['LotFrontage'] = df['LotFrontage'].astype(int)

train=pd.get_dummies(data=df)
resultant_df=pd.DataFrame(train.corr())
highly_corr =resultant_df[abs(resultant_df['SalePrice'])>0.50]['SalePrice']
highly_corr_columns=list(highly_corr.index)
#applying log to tackle skewed data
#train = train.drop(train[train['TotalBsmtSF']==0].index, axis =0)
#train.loc[:,['SalePrice','TotalBsmtSF','GrLivArea','YearBuilt']] = np.log(train[['SalePrice','TotalBsmtSF','GrLivArea','YearBuilt']])


#print(len(highly_corr))
labels_to_drop = ['GarageArea','TotRmsAbvGrd','1stFlrSF', 'KitchenQual_TA','ExterQual_TA','BsmtQual_Ex','KitchenQual_Ex','YearRemodAdd']
train=train[highly_corr_columns]
train = train.drop(labels_to_drop, axis =1)
columns_considered=list(train.columns)
#print(len(highly_corr_columns))

#Removing Outliers
train =train.drop(train[train.GrLivArea == 5642].index)
train =train.drop(train[train.GrLivArea == 4476].index)
train =train.drop(train[train.SalePrice == 755000].index)
train =train.drop(train[train.SalePrice == 745000].index)#GrlivArea will delete this entry
train =train.drop(train[train.TotalBsmtSF == 6110].index)#GrlivArea will delete this entry
train =train.drop(train[train.YearBuilt == 1872].index)
train =train.drop(train[train.YearBuilt == 1875].index)
#print(len(train))
#print(train.info())



# Cleaning Test Data
test=pd.read_csv('test.csv')
test_id=test['Id']
columns_considered.remove('SalePrice')
test=test[columns_considered]

test['GarageCars'] = test['GarageCars'].fillna((test['GarageCars'].mean()))
test['TotalBsmtSF'] = test['TotalBsmtSF'].fillna((test['TotalBsmtSF'].mean()))
test[['TotalBsmtSF','GarageCars']] = test[['TotalBsmtSF','GarageCars']].astype(np.int64)
#test[['TotalBsmtSF']] = test[['TotalBsmtSF']].replace(0,1)
#test.loc[:,['TotalBsmtSF','GrLivArea', 'YearBuilt']] = np.log(test[['TotalBsmtSF','GrLivArea','YearBuilt']])
#print(test.info())
#print(test.isnull().sum())

#model
#clf=RandomForestRegressor(random_state=42)



#optimizing model
param_grid = [{"n_estimators": np.arange(10, 100, 2)},
              {"max_depth": np.arange(1, 28, 1)},
              {"min_samples_split": np.arange(10,40)},
              {"min_samples_leaf": np.arange(1,60,1)},
              {"max_leaf_nodes": np.arange(2,60,1)},
              {"min_weight_fraction_leaf": np.arange(0.1,0.4, 0.1)}]

#min_samples_split={'min_samples_split':np.arange(10,40)}
print(train.info())
y=train['SalePrice']
X=train.drop('SalePrice',axis=1)
X=np.array(X)
#X=preprocessing.scale(X)

clf=RandomForestRegressor(random_state=42)
linear=LinearRegression()
'''
for param in param_grid:
    #print(type(param))
    model = GridSearchCV(clf, param_grid=param, cv=5)
    model.fit(X,y)
    print("Best value for" ,model.best_params_)
'''
#print("Best score for" ,model.best_score_)
#Applying optimized values
#optimized_clf=RandomForestRegressor(random_state=42,n_estimators=94,max_depth=7,min_samples_split=19,min_samples_leaf=8,
 #                         min_weight_fraction_leaf=0.1)
X_train,X_test,y_train,y_test=train_test_split(X,y,random_state=42,train_size=0.8)

clf.fit(X_train,y_train)
linear.fit(X_train,y_train)
y_pred=linear.predict(X_test)
print((np.sqrt(mean_squared_error(y_test, y_pred,multioutput='raw_values'))))

results=clf.predict(test)
print('LinearRegression Score :',linear.score(X_test,y_test))
print('RandomTree Score :',clf.score(X_test,y_test))

resultant_df=pd.DataFrame({'Id':test_id,'SalePrice':results})
resultant_df.to_csv('C:\\Users\\Deepanshu\\PycharmProjects\\Kaggle\\Housing Price\\results.csv',index=False)
