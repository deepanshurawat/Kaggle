import numpy as np
import pandas as pd
import re as re
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import  matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier,DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier

df=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')
original_df=df
original_test=test
dataset=[df,test]
#print(df.isnull().sum())

#feature engineering
#1 Sex (not required anything new)
#print(df.groupby('Sex')['Sex','Survived'].mean())

#2 Pclass (not required anything new)
#print(df.groupby('Pclass')['Pclass','Survived'].mean())

#3 Embarked (not required anything new)
for data in dataset:
    data['Embarked'] = data['Embarked'].fillna('S')
#print(df.groupby('Embarked')['Embarked','Survived'].mean())

#4 Sibsp (not required anything new)
#print(df.groupby('SibSp')['SibSp','Survived'].mean())

#5 Parch (not required anything new)
#print(df.groupby('Parch')['Parch','Survived'].mean())

#6 introducing new variable family size which is basically sum of sibling and children
for data in dataset:
    data['FamilySize'] = data['SibSp'] + data['Parch'] + 1

#print (df[['FamilySize', 'Survived']].groupby(['FamilySize'], as_index=False).mean())

#7 introducing new variable is_alone which tell about whether person has family or not
for data in dataset:
    data['IsAlone'] = 0
    data.loc[data['FamilySize'] == 1, 'IsAlone'] = 1
#print(df.groupby('isalone')['isalone','Survived'].mean())

#8 Titles

def get_title(name):
	title_search = re.search(r'([A-Za-z]+)\.', name)
	# If the title exists, extract and return it.
	if title_search:
		return title_search.group(1)
	return ""
for data in dataset:
    data['Title'] = data['Name'].apply(get_title)

data['Title'] = data['Title'].replace(['Lady', 'Countess', 'Capt', 'Col', \
                                       'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

data['Title'] = data['Title'].replace('Mlle', 'Miss')
data['Title'] = data['Title'].replace('Ms', 'Miss')
data['Title'] = data['Title'].replace('Mme', 'Mrs')


#print(pd.crosstab(df['Title'], df['Sex']))
#for data in dataset:


#print (df[['Title', 'Survived']].groupby(['Title'], as_index=False).mean())



#9 Fare
for data in dataset:
    data['Fare'].fillna(df["Fare"].mean(), inplace=True)
    data['Fare'].replace(to_replace=0,value=df.groupby("Pclass")["Fare"].transform(np.mean),inplace=True)
    df['CategoricalFare'] = pd.qcut(df['Fare'], 4)
#print(df.groupby('CategoricalFare')['CategoricalFare','Survived'].mean())

#10 Age
#print(df.info())
#print(df.groupby("Title")['Title','Age'].apply(np.mean))
for data in dataset:
    data['Age'].fillna(df.groupby("Title")['Age'].transform(np.mean), inplace=True)

#print(df.groupby(["Title","Age"])["Age"].transform(np.mean))
df['CategoricalAge'] = pd.qcut(df['Age'], 5)
#print(df.groupby('CategoricalAge')['CategoricalAge','Survived'].mean())
#print(df.isnull().sum())

title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}

#Mapping Data
for data in dataset:
    # Mapping Titles
    data['Title'] = data['Title'].map(title_mapping)
    data['Title'] = data['Title'].fillna(0)

    # Mapping Sex
    data['Sex'] = data['Sex'].map({'female': 0, 'male': 1}).astype(int)

    # Mapping Embarked
    data['Embarked'] = data['Embarked'].map({'S': 0, 'C': 1, 'Q': 2}).astype(int)

    # Mapping Fare
    data.loc[data['Fare'] <= 7.925, 'Fare'] = 0
    data.loc[(data['Fare'] > 7.925) & (data['Fare'] <= 14.5), 'Fare'] = 1
    data.loc[(data['Fare'] > 14.5) & (data['Fare'] <= 31.275), 'Fare'] = 2
    data.loc[data['Fare'] > 31.275, 'Fare'] = 3
    #data['Fare'] = data['Fare'].astype(int)

    # Mapping Age
    data.loc[data['Age'] <= 20, 'Age'] = 0
    data.loc[(data['Age'] > 20) & (data['Age'] <= 26), 'Age'] = 1
    data.loc[(data['Age'] > 26) & (data['Age'] <= 32), 'Age'] = 2
    data.loc[(data['Age'] > 32) & (data['Age'] <= 38), 'Age'] = 3
    data.loc[data['Age'] > 38, 'Age'] = 4

# Feature Selection
pd.crosstab(df.Pclass,df.Survived,margins=True).style.background_gradient(cmap='summer_r')

drop_elements = ['PassengerId', 'Name', 'Ticket', 'Cabin','Parch','SibSp'] #,,'FamilySize'

df = df.drop(drop_elements, axis=1)
df = df.drop(['CategoricalAge', 'CategoricalFare'], axis=1)

test = test.drop(drop_elements, axis=1)

training_df=df.drop(['Survived'],axis=1)
y = np.array(df['Survived'])

#featureset X
#print(training_df.isnull().sum())
#print(training_df.head())

X=np.array(training_df)
print(len(X))
X=preprocessing.scale(X)
X_to_be_test=np.array(test)
X_to_be_test=preprocessing.scale(X_to_be_test)
X_train,X_test,y_train,y_test=train_test_split(X,y,random_state=42,train_size=0.80)
#clf=SVC()
clf=RandomForestClassifier(random_state=42,criterion='entropy')
clf.fit(X_train,y_train)

#Measuring accuracy in training
y_pred=clf.predict(X_test)
print(len(y_pred))
print(accuracy_score(y_test, y_pred))
#print(correct/len(X))
results=clf.predict(X_to_be_test)
a=confusion_matrix(y_test, y_pred)
print(a)
#print(results)

resultant_df=pd.DataFrame({'PassengerId':original_test['PassengerId'],'Survived':results})
resultant_df.to_csv('C:\\Users\\Deepanshu\\PycharmProjects\\Kaggle\\Titanic\\results.csv',index=False)
#print('new commit')